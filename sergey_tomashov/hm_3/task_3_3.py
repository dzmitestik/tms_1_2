# В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"

start_name = 'Ivanou Ivan'
name = start_name.split()
name = name[::-1]
reverse_name = ' '.join(name)

'''
One line solution:
reverse_name = ' '.join(name.split()[::-1])
'''

print(f'Name before: {start_name}\t Name after: {reverse_name}')
